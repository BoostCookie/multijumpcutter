#!/usr/bin/env python3

import argparse
import subprocess
import os
import shutil
from pathlib import Path

args = None
jumpcutter_custom_args = None
old_workdir = Path(os.getcwd())

def get_args():
    parser = argparse.ArgumentParser(fromfile_prefix_chars="@", description="Modifies [multiple] video files to play at different speeds when there is sound vs. silence.")

    # custom for this program
    parser.add_argument('--temp_dir', type=str, help="the directory in which the conversion takes place")
    parser.add_argument('--output_dir', type=str, default="cut", help="the directory where the output files are being created")
    parser.add_argument('--jumpcutter', type=str, help="path to the jumpcutter executable")

    # changed to support multiple files/urls
    parser.add_argument('--input_files', type=str,  help='the video file you want modified', action="append", nargs="*")
    #parser.add_argument('--urls', type=str, help='A youtube url to download and process', action="append", nargs="*")

    return parser.parse_known_args()

def printargs(args):
    res = ""
    for arg in args:
        res += "{} ".format(arg.replace(" ", r"\ "))
    print(res[:-1])

# chars in filenames that jumpcutter cannot handle
escapechars_list = [" ", "'"]
def escapechars(string):
    for escapechar in escapechars_list:
        string = string.replace(escapechar, "\\" + escapechar)
    return string

def escapechar_in_str(string):
    for escapechar in escapechars_list:
        if escapechar in string:
            return True
    return False

def remove_escape_chars(string):
    for escapechar in escapechars_list:
        string = string.replace(escapechar, "")
    return string

def deleteTEMP():
    temp = Path("TEMP")
    if temp.exists():
        while True:
            print("Delete {}? [Y/n] ".format(temp.resolve()), end="")
            user_input = input()
            if "n" in user_input.lower():
                raise Exception("{} needs to be empty".format(temp.resolve()))
            elif (user_input.lower() == "y") or (not user_input):
                print("rm -r {}".format(escapechars(str(temp))))
                shutil.rmtree(temp)
                break
            else:
                print("Invalid input.")

# takes a single file and a directory of files as argument
def jumpcutter(inputfiles):
    global args
    global jumpcutter_custom_args
    global old_workdir

    inputfiles = Path(inputfiles)

    if inputfiles.is_dir():
        for inputfile in inputfiles.iterdir():
            jumpcutter(inputfile)
        return

    inputfile = Path(inputfiles)

    if (args.temp_dir) and (not inputfile.is_absolute()):
        inputfile = old_workdir.joinpath(inputfile)

    temp_inputfile = inputfile
    if escapechar_in_str(str(inputfile)):
        temp_inputfile = Path(remove_escape_chars(inputfile.name))
        print("cp {} {}".format(escapechars(str(inputfile)), escapechars(str(temp_inputfile))))
        shutil.copyfile(str(inputfile), str(temp_inputfile))

    outputfilepath = Path(args.output_dir)
    if (args.temp_dir) and (not outputfilepath.is_absolute()):
        outputfilepath = old_workdir.joinpath(outputfilepath)
    print("mkdir -p {}".format(escapechars(str(outputfilepath))))
    outputfilepath.mkdir(exist_ok=True)
    outputfilepath = outputfilepath.joinpath(temp_inputfile.name)
    temp_outputfile = outputfilepath
    if inputfile == outputfilepath:
        outputfilepath = outputfilepath.parent.joinpath(outputfilepath.stem + ".cut" + outputfilepath.suffix)
    if escapechar_in_str(str(outputfilepath)):
        temp_outputfile = Path(outputfilepath.stem + ".cut" + outputfilepath.suffix)

    jumpcutter_args = []
    if args.jumpcutter:
        jumpcutter_path = old_workdir.joinpath(args.jumpcutter)
        if os.name == "nt":
            jumpcutter_args.append("py")
        elif os.name == "posix":
            jumpcutter_args.append("python3")
        else:
            raise Exception("OS {} not supported".format(osname))
        jumpcutter_args.append(str(jumpcutter_path))
    else:
        jumpcutter_args.append("jumpcutter")

    jumpcutter_args += ["--input_file", str(temp_inputfile), "--output_file", str(temp_outputfile)] + jumpcutter_custom_args
    printargs(jumpcutter_args)
    subprocess.run(jumpcutter_args)

    if temp_outputfile != outputfilepath:
        print("mv {} {}".format(escapechars(str(temp_outputfile)), escapechars(str(outputfilepath))))
        shutil.move(str(temp_outputfile), str(outputfilepath))
    if temp_inputfile != inputfile:
        print("rm {}".format(escapechars(str(temp_inputfile))))
        temp_inputfile.unlink()

def main():
    global args
    global jumpcutter_custom_args
    global old_workdir

    args, jumpcutter_custom_args = get_args()

    # change current working directory if --temp_dir was given
    if args.temp_dir:
        temp_dir = Path(args.temp_dir)
        if temp_dir.is_file():
            raise Exception("temp_dir \"{}\" is a file".format(temp_dir))
        print("mkdir -p {}".format(escapechars(str(temp_dir))))
        temp_dir.mkdir(exist_ok=True)

        print("cd {}".format(escapechars(str(temp_dir))))
        os.chdir(temp_dir)

    deleteTEMP()

    for inputfiles_list in args.input_files:
        for inputfiles in inputfiles_list:
            jumpcutter(inputfiles)

    if args.temp_dir:
        print("cd {}".format(escapechars(str(old_workdir))))
        os.chdir(old_workdir)
    if not os.listdir(args.temp_dir):
        print("rmdir {}".format(escapechars(str(args.temp_dir))))
        Path(args.temp_dir).rmdir()

    # TODO: add support for urls

if __name__ == "__main__":
    main()

