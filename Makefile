.Phony: install, uninstall, default, howto

default: howto

install:
	cp src/multijumpcutter.py /usr/bin/multijumpcutter

uninstall:
	rm /usr/bin/multijumpcutter

howto:
	$(info Use `sudo make install` or `sudo make uninstall`)
