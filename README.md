# multijumpcutter

Support for multiple input files with carykh's jumpcutter.

Dependencies: [jumpcutter](https://github.com/carykh/jumpcutter)

## How to use
It has all the arguments as the original, except for the following differences:

- Instead of `--input_file` it has `--input_files` which takes both directories and files.
- Instead of `--output_file` it has `--output_dir` which is `./cut` by default
- It has `--jumpcutter` which is the path to the original `jumpcutter.py`. Not needed if you've installed [jumpcutter from the AUR](https://aur.archlinux.org/packages/jumpcutter-git/) or `jumpcutter`is part of your `$PATH`.
- It has `--temp_dir` which is the directory where `jumpcutter` creates its `TEMP` directory

Via the `@` symbol files can also be used to specify arguments.
For example place a file `args.txt` somewhere with the content
```
--silent_threshold
0.04
--sounded_speed
1.4
--silent_speed
10
--jumpcutter
/my/path/to/jumpcutter.py
```
and then run the program for example with

`multijumpcutter @path/to/args.txt --input_files path/to/file.mp4 --output_dir my/videosdir`

## Install
On Linux you can install multijumpcutter (copying it to `/usr/bin/`) with the Makefile:
`sudo make install`.
Uninstalling it (deleting `/usr/bin/multijumpcutter`) works with `sudo make uninstall`.

## TODO

- Support URLs

